<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function SupplierProducts(){
        return $this->belongsToMany('App\SupplierProduct')->withPivot("cartoon","qty","rate","total");
    }
    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }
}
