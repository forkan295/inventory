<?php

namespace App;

use App\Product;
use App\Customer;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $guarded = [];

    public function Customer()
    {
      return  $this->belongsTo(Customer::class);
    }
    public function Products()
    {
      return  $this->belongsToMany(Product::class)->withPivot('cartoon','qty','rate','total');
    }
}
