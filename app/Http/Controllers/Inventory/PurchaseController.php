<?php

namespace App\Http\Controllers\Inventory;

use App\Product;
use App\Category;
use App\Purchase;
use App\Supplier;
use App\SupplierProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['purchases'] = Purchase::all();
        return view('admin.pages.purchase.allPurchase', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
        $data['suppliers']=Supplier::all();
        return view('admin.pages.purchase.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
          "supplier"=>"required",
          "purchase_date"=>"required",
          "purchase_no"=>"required",
          "details"=>"required",
          "payment_type"=>"required",
          "grand_total"=>"required",
          "product_id"=>"required",
          "cartoon"=>"required",
        ]);

        $purchase = new Purchase;
        $purchase->supplier_id = $request->supplier;
        $purchase->purchase_date = $request->purchase_date;
        $purchase->purchase_no = $request->purchase_no;
        $purchase->purchase_details = $request->details;
        $purchase->payment_type = $request->payment_type;
        $purchase->grand_total = $request->grand_total;
        $purchase->save();
        
         
        for($i = 0;$i<count($request->product_id);$i++){

            // dd($request->cartoon[$i]);
                    
            $purchase->SupplierProducts()->attach($request->product_id[$i],['cartoon'=> $request->cartoon[$i],
            'qty'=> $request->qty[$i],
            'rate'=> $request->rate[$i],
            'total'=> $request->total[$i]]);

           

           $productExist =  Product::where('supplier_product_id',$request->product_id[$i])->first();
        //    dd($productExist);
           $supplierProduct = SupplierProduct::find($request->product_id[$i]);

           if($productExist != null)
           {
            $productExist->cartoon += $request->cartoon[$i];
            $productExist->qty += $request->qty[$i];
            $productExist->rate = $request->rate[$i];
            $productExist->total += $request->total[$i];
            $productExist->save();
            
           }
           if($productExist == null){
            $product = new Product;

            if(Storage::disk('public')->exists('productimage/'.$supplierProduct->product_img))
                    {
                        if(!Storage::disk('public')->exists('stockProductImage'))
                            {
                                Storage::disk('public')->makeDirectory('stockProductImage');
                            }
                        Storage::copy(('public/productimage/'.$supplierProduct->product_img),('public/stockProductImage/'.$supplierProduct->product_img));
                    }

            $product->supplier_product_id = $request->product_id[$i];
            $product->name = $supplierProduct->name;
            $product->model_name = $supplierProduct->model_name;
            $product->category_id = $supplierProduct->category_id;
            $product->ppc = $supplierProduct->ppc;
            $product->supplier_price = $supplierProduct->supplier_price;
            $product->sell_price = $supplierProduct->sell_price;
            $product->product_img = $supplierProduct->product_img;
            $product->cartoon = $request->cartoon[$i];
            $product->qty = $request->qty[$i];
            $product->rate = $request->rate[$i];
            $product->total = $request->total[$i];
            $product->save();
           }
            




        };


                                                                 
        return redirect()->back();                                                            

        




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = Purchase::find($id);
        

            return $purchase->SupplierProducts;
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $purchase = Purchase::find($id);
        $purchase->SupplierProducts()->detach();
        $purchase->delete();
        return redirect()->back();
    }

    public function supplierItem($id)
    {
        $supplier = Supplier::find($id);
        $items = $supplier->supplierProducts()->get();
        return $items;
    }
    public function products(){
        $data['products']=Product::all();
        $data['categories']=Category::all();
        return view('admin.pages.product.allProduct',$data);
    }
}
