<?php

namespace App\Http\Controllers\Inventory;

use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['suppliers']= Supplier::all();
        return view('admin.pages.supplier.allSupplier',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([

        "name"=>'required',
        "mobile"=>'required',
        "address"=>'required',
        "details"=>'required'

       ]);

       $supplier = new Supplier;
       $supplier->name = $request->name;
       $supplier->mobile = $request->mobile;
       $supplier->address = $request->address;
       $supplier->details = $request->details;
       $supplier->save();
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            "name"=>'required',
            "mobile"=>'required',
            "address"=>'required',
            "details"=>'required|max:500'
    
           ]);
    
           $supplier = Supplier::find($id);
           $supplier->name = $request->name;
           $supplier->mobile = $request->mobile;
           $supplier->address = $request->address;
           $supplier->details = $request->details;
           $supplier->save();
           return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->delete();
        return redirect()->back();

    }
}
