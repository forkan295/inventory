<?php

namespace App\Http\Controllers\Inventory;

use App\Product;
use App\Category;
use App\Supplier;
use App\SupplierProduct;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class SupplierProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories']=Category::all();
        $data['suppliers'] = Supplier::all();
        $data['products'] = SupplierProduct::all();
        return view('admin.pages.supplier-product.allSupplierProduct',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories']=Category::all();
        $data['suppliers'] = Supplier::all();
        return view('admin.pages.supplier-product.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            "name"=>"required",
            "model_name"=>"required",
            "ppc"=>"required",
            "supplier_price"=>"required",
            "sell_price"=>"required",
            ]);
            $supplier_product= new SupplierProduct;
            $supplier_product->name = $request->name;
            $supplier_product->model_name = $request->model_name;
            $supplier_product->ppc = $request->ppc;
            $supplier_product->supplier_price = $request->supplier_price;
            $supplier_product->sell_price = $request->sell_price;
            $supplier_product->category_id = $request->product_category;
            
            if($request->hasFile('product_img'))
            {
            $request->validate([
                "product_img" => 'image|mimes:jpeg,png',
                ]);

            if(!Storage::disk('public')->exists('productImage'))
            {
                Storage::disk('public')->makeDirectory('productImage');
            } 
            
            $file = $request->file('product_img');
            $file_ext = $file->getClientOriginalExtension();
            $image = 'image_'.time().'.'.$file_ext;
            $save = 'storage/productImage/'. $image;
            Image::make($file)->fit(500,300)->save($save);
        }
        else
        {
            $image = null;
        }

        $supplier_product->product_img = $image;
        $supplier_product->save();
        // dd($request->supplier_name);
        $supplier_product->suppliers()->sync($request->supplier_name);
        return redirect()->back();
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $request->validate([
            "name"=>"required",
            "model_name"=>"required",
            "ppc"=>"required",
            "supplier_price"=>"required",
            "sell_price"=>"required",
            ]);
            $supplier_product= SupplierProduct::find($id);
            $supplier_product->name = $request->name;
            $supplier_product->model_name = $request->model_name;
            $supplier_product->ppc = $request->ppc;
            $supplier_product->supplier_price = $request->supplier_price;
            $supplier_product->sell_price = $request->sell_price;
            $supplier_product->category_id = $request->product_category;
            
            if($request->hasFile('product_img'))
            {
            $request->validate([
                "product_img" => 'image|mimes:jpeg,png',
                ]);

                if($supplier_product->product_img != null)
                {
                    if(Storage::disk('public')->exists('productimage/'.$supplier_product->product_img))
                    {
                        Storage::disk('public')->delete('productimage/'.$supplier_product->product_img);
                    }
                }
            
            $file = $request->file('product_img');
            $file_ext = $file->getClientOriginalExtension();
            $image = 'image_'.time().'.'.$file_ext;
            $save = 'storage/productImage/'. $image;
            Image::make($file)->fit(500,300)->save($save);
        }
        else
        {
            $image = $supplier_product->product_img;
        }

        $supplier_product->product_img = $image;
        $supplier_product->save();
        // dd($request->supplier_name);
        $supplier_product->suppliers()->sync($request->supplier_name);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = SupplierProduct::find($id);
        $outProducts = Product::where('supplier_product_id',$id)->first();

        if(!$outProducts){
                    if(Storage::disk('public')->exists('productimage/'.$product->product_img))
                {
                    Storage::disk('public')->delete('productimage/'.$product->product_img);
                }
                
                foreach($product->suppliers as $supplier){

                    $product->suppliers()->detach($supplier->id);
                }
                
                $product->delete();
                return redirect()->back();
        }else{

                return redirect()->back();
        }
    }
}
