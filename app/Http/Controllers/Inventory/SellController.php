<?php

namespace App\Http\Controllers\Inventory;

use App\Sell;
use App\Product;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['sells'] = Sell::all();
        return view('admin.pages.sell.allsell', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = Customer::all();
        return view('admin.pages.sell.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            "customer" => "required",
            "date" => "required",
            "details" => "required",
            "payment_type" => "required",
            "grand_total" => "required",

            "product_id" => "required",
            "av_cartoon" => "required",
            "cartoon" => "required|array",
            "cartoon.*" => "filled|max:255",
            "ppc" => "required",
            "qty" => "required",
            "rate" => "required",
            "total" => "required",
        ]);

        // $sell = new Sell;
        $sell =  Sell::create([
            "customer_id" => $request->customer,
            "date" => $request->date,
            "details" => $request->details,
            "payment_type" => $request->payment_type,
            "grand_total" => $request->grand_total,
            "due" => $request->due,
            "paid" => $request->paid,

        ]);
        // dd($sell->id);

        $customer = Customer::find($request->customer);
        $customer->due += $request->due;
        $customer->save();

        for ($i = 0; $i < count($request->product_id); $i++) {

            // dd($request->cartoon[$i]);

            $sell->Products()->attach($request->product_id[$i], [
                'cartoon' => $request->cartoon[$i],
                'qty' => $request->qty[$i],
                'rate' => $request->rate[$i],
                'total' => $request->total[$i]
            ]);





            $product =  Product::where('id', $request->product_id[$i])->first();
            $product->cartoon = $request->av_cartoon[$i];
            $product->qty -= $request->qty[$i];
            $product->save();

            //    //    dd($productExist);

            //       if($productExist != null)
            //       {
            //        $productExist->cartoon += $request->cartoon[$i];
            //        $productExist->qty += $request->qty[$i];
            //        $productExist->rate = $request->rate[$i];
            //        $productExist->total += $request->total[$i];
            //        $productExist->save();

            //       }
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sell = Sell::find($id);


        return $sell->Products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['sells'] = Sell::find($id);
        $data['products'] = Sell::find($id)->Products;
        $data['customers'] = Customer::all();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sell = Sell::find($id);
        $sell->Products()->detach();
        $sell->delete();
        return redirect()->back();
    }
    public function getProduct($name)
    {

        $product = Product::where('name', 'like', "%{$name}%")->orWhere('model_name', 'like', "%{$name}%")->get();

        return $product;
    }

    public function getPaid()
    {
      $data['sells'] =  Sell::where('due',0)->get();
      return view('admin.pages.sell.allsell',$data);
    }
    public function getDue()
    {
      $data['sells'] =  Sell::where('due','!=',0)->get();
      return view('admin.pages.sell.allsell',$data);
    }
}
