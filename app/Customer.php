<?php

namespace App;

use App\Sell;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];
    public function Sells()
    {
      return  $this->hasMany(Sell::class);
    }
}
