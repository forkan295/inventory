<?php

namespace App;

use App\Sell;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Sells()
    {
      return  $this->belongsToMany(Sell::class)->withPivot('cartoon','rate','total');
    }
}
