<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function supplierProducts()
    {
      return $this->belongsToMany('App\SupplierProduct');
    }
    public function Purchases(){
      return $this->hasMany('App\Purchase');
  }
}
