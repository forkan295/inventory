<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierProduct extends Model
{
    public function suppliers()
    {
      return $this->belongsToMany('App\Supplier');
    }
    public function category(){
      return $this->belongsTo('App\Category');
  }
    public function purchases(){
      return $this->belongsToMany('App\Purchase')->withPivot("cartoon","qty","rate","total");
  }
}
