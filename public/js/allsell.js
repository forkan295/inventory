$(document).ready(function() {

    $.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});
// Ajax call pressing view button start
$('.viewModalBtn').click(function(e) {
e.preventDefault();

// alart('hello');
var data=$(this).data('id');
// var action ="{{ route('category.edit',"id") }}";
var url = window.location.origin;
// var url = '{{URL::to('class_routine')}}';
$.ajax({
  type : 'GET',
  url : url+'/Inventory/sell/'+data,
  data : {'id':data},
  
  success:function(data){

    // $('#cat_name').val(data.cat_name);
    var td = "";

    

    for(var i = 0 ;i<data.length;i++)
    {
      td += 
     `
        <tr>
          <th scope="row">${i+1}</th>
          <td>${data[i].name}</td>
          <td><img src='${url}/storage/stockProductImage/${data[i].product_img}' alt="" class="img-fluid "></td>
          <td>${data[i].pivot.cartoon}</td>
          <td>${data[i].ppc}</td>
          <td>${data[i].pivot.qty}</td>
          <td>${data[i].pivot.rate}</td>
          <td>${data[i].pivot.total}</td>    
        </tr>
        `

    }
    $("#productView").html(td);
    console.log(data);
 
    // $('.class-id').val(data.class_id);
    // $('.subject_id').val(data.subject_id);
    // $('.day_of_week').val(data.day_of_week);
    // $('.teacher_id').val(data.teacher_id);
    // $('.start_time').val(data.start_time);
    // $('.end_time').val(data.end_time);
    // $('.classFormUpdate').attr('action',action);
    $('#editModal').modal('show');
  }
});
});
// Ajax call pressing view button end

// Ajax call pressing Edit button start
$('.editModalBtn').click(function(e) {
e.preventDefault();



// alart('hello');
var data=$(this).data('id');
// var action ="{{ route('category.edit',"id") }}";
var url = window.location.origin;
// var url = '{{URL::to('class_routine')}}';
$.ajax({
  type : 'GET',
  url : url+'/Inventory/sell/'+data+'/edit',
  data : {'id':data},
  
  success:function(data){
    

  //   $( "#edit" ).on('shown.bs.modal', function(){
  
  //  });

    var select = $('#selectbox1');
    select.children().remove();

    var date = $('.date');
    var details = $('.details');
    var payment = $('.payment');

    payment.children().remove();

    for(var i = 0;i<data.customers.length;i++)
    {
      console.log(data.customers[i].id);
      var options = `<option value="${data.customers[i].id}" ${data.sells.customer_id == data.customers[i].id ? "selected" : " "}> ${ data.customers[i].name}</option>`;
      select.append(options);
    }
    var payOptions = `<option value="card" ${data.sells.payment_type == "card"? "selected" : " "}> Card</option>
    <option value="cash" ${data.sells.payment_type == "cash" ? "selected" : " "}>Cash</option>
    `;
    payment.append(payOptions);

    date.val(data.sells.date)
    details.val(data.sells.details)

    // retrive the sell products for edit table in edit page
    var tbody = $('.tbody');
    tbody.children().remove();

   $.each(data.products, function (key, val) {

          var row = `<tr class="firsttr" >
                          <td style="width:200px">
                            <div class="form-group">
                              <input type="search" class="sellProduct form-control" value="${val.name}">
            
                              <ul class="ul d-none"></ul>
                            </div>
                          </td>
                          <td class="av">
                            <input readonly type="number" class="form-control av-cartoon" name="av_cartoon[]" value="${val.cartoon}" data-avcartoon = "${val.cartoon}">
                          </td>
                          <td>
                            <input  type="number" class="form-control cartoon" name="cartoon[]" value="${val.pivot.cartoon}" onkeyup="myFunction()" min='0' max = '${val.cartoon + val.pivot.cartoon}' data-cartoon = "${val.pivot.cartoon}">
                          </td>
                          <td>
                            <input type="text" class="form-control ppc" name="ppc[]" readonly value="${val.ppc}">
                          </td>
                          <td>
                            <input type="text" class="form-control qty"  name="qty[]" readonly value="${val.pivot.qty}"> 
                          </td>
                          <td>
                            <input type="text" class="form-control rate" name="rate[]" value="${val.pivot.rate}">
                          </td>
                          <td>
                            <input type="text" class="form-control total"  name="total[]" readonly value="${val.pivot.total}">
                          </td>
                          <td>
                           <button type="button" class="btn btn-danger" >Delete</button>
                          </td>
                        </tr>`;

          tbody.append(row);
              

          window.myFunction = () => {
             var ee = event.target
             var thisCartoon = $(this);
             var av_cartoonsTd = ee.closest(".firsttr").children;
             var av_cartoon = parseInt(av_cartoonsTd[1].children[0].value);
             
             var total_cartoon = parseInt(ee.value) + av_cartoon;
             var thisVal = parseInt(ee.dataset.cartoon);
             var av_const = parseInt(av_cartoonsTd[1].children[0].dataset.avcartoon);
             console.log(thisVal + av_const);

             

             if(ee.value <= (thisVal + av_const))
              {
               av_cartoon--;
                  if(av_cartoon <= 0)
                  {
                    av_cartoon = 0;     
                  }
                  
               av_cartoonsTd[1].children[0].value = av_cartoon;

              }else{
                ee.value = thisVal + av_const;
                av_cartoonsTd[1].children[0].value = 0;
              }
              

            // var thisVal = val.pivot.cartoon;
            // var totalCartoon = parseInt(val.cartoon)+parseInt(thisVal);
            // var av_cartoonsTd = thisCartoon.closest(".firsttr").children;
            // var av_cartoon = av_cartoonsTd.children;
            // console.log(av_cartoon);
           
            // if(ee.value < 0)
            // {
            //   ee.value = 0;
            // }
            // // alert(thisCartoon.val() + "=" + totalCartoon);
            // if(thisCartoon.val() >= totalCartoon)
            // {
            //   thisCartoon.val(totalCartoon);
            // }else{
            //   // av_cartoon.value++;
            // }
        };
            //   var cartoon = $('.cartoon');
              
            //   cartoon.keyup(function(){
            //     var thisCartoon = $(this);
            //     var thisVal = val.pivot.cartoon;
            //     var totalCartoon = parseInt(val.cartoon)+parseInt(thisVal);
            //     var av_cartoonsTd = thisCartoon[0].closest(".firsttr").children;
            //     var av_cartoon = av_cartoonsTd[1].children[0];
            //     console.log($(this).parent());
               
            //     if(thisCartoon.val() < 0)
            //     {
            //       thisCartoon.val(0);
            //     }
            //     // alert(thisCartoon.val() + "=" + totalCartoon);
            //     if(thisCartoon.val() >= totalCartoon)
            //     {
            //       thisCartoon.val(totalCartoon);
            //     }else{
            //       // av_cartoon.value++;
            //     }
            //     // console.log(av_cartoon);
            // })
                
        });

        
              

    
   
    // console.log(select);
    console.log(data);

    // $('.class-id').val(data.class_id);
    // $('.subject_id').val(data.subject_id);
    // $('.day_of_week').val(data.day_of_week);
    // $('.teacher_id').val(data.teacher_id);
    // $('.start_time').val(data.start_time);
    // $('.end_time').val(data.end_time);
    // $('.classFormUpdate').attr('action',action);
    $('#editModal').modal('show');
  }

});


});

// Ajax call pressing Edit button end


if(window.location.href === "http://inventory.test/Inventory/sell")
{
$('.all').addClass('active');
} else if(window.location.href === "http://inventory.test/Inventory/sell-paid")
{
$('.paid').addClass('active');
}else{
$('.due').addClass('active');
}

});
