$(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

  //Initialize Select2 Elements
     $('.select2bs4').select2({
   theme: 'bootstrap4'
   })

   //sellProduct function call
   function sellProduct()
      {
       $('.sellProduct').keyup(function(){
        var data = $(this).val();
        var url = window.location.origin;
        var ul = $(this).siblings();
       ul.removeClass('d-none');
            if(data != 0){
             
                   $.ajax({
                   type:"GET",
                   url : url+'/Inventory/product-get/'+data,
                   data : {data:data},
                   success : function(response) {
                     ul.children().remove();
                       data = response;
                       console.log(data);

                       $.each (data, function (key, val) {
                         
                         var liTag = `<li class='li' value="${val.id}"> ${val.name}</li>`
                         ul.append(liTag);

                       });

                       $('.li').click(function(){
                         var id = $(this).val();
                         var inputs = $(this).parents("td").siblings();
                         var siblingsOfParent = $(this).parent().siblings();
                         siblingsOfParent.val($(this).text());

                         var hiddenInput = siblingsOfParent.parent();
                         hiddenInput.append(`<input type="search" class="sellProduct form-control" hidden name="product_id[]" value = "${id}">`);

                         ul.children().remove();
                         ul.addClass('d-none');

                         $.each(data, function (key, val) 
                         {

                             if(id == val.id)
                             {
                               var av_cartooon = inputs.find('.av-cartoon');
                               av_cartooon.val(val.cartoon);
                               
                               var cartoon = inputs.find('.cartoon');
                               
                               inputs.find('.ppc').val(val.ppc);
                               
                               cartoon.keyup(function(){
                                 var thisCartoon = $(this);
                                 if(thisCartoon.val() < 0)
                                 {
                                   thisCartoon.val(0);
                                 }
                                 if(thisCartoon.val() >= val.cartoon)
                                 {
                                   thisCartoon.val(val.cartoon);
                                 }
                                 inputs.find('.av-cartoon').val(val.cartoon - thisCartoon.val());
                                 var rate = inputs.find('.rate');
                                 var qty = inputs.find('.qty');
                                 // var sellPrice = val.sell_price;
                                     inputs.find('.qty').val($(this).val()*val.ppc);
                                     rate.keyup(function(){
                                         sellPrice = $(this).val()*qty.val();
                                         inputs.find('.total').val(Math.round(sellPrice));
                                     })
                                     inputs.find('.total').val(Math.round(qty.val()*rate.val()));
                                     var grndTotal = $('.total');
                                     console.log(grndTotal);
                                     var grndtotal = 0;
                                     $.each(grndTotal,function(key,value){
                                       grndtotal += parseInt(value.value);
                                     })
                                     $('.grandTotal').val(grndtotal);

                                     if ($(this).val() != 0) {
                                         $('.sbmt').attr('disabled',false);
                                         }else{
                                           $('.sbmt').attr('disabled',true);
                                         }
                                 })
                                 var rt = (val.sell_price/val.ppc).toFixed(3);
                               inputs.find('.rate').val(rt);
                               
                             }
                         });

                       })
                     
                   },
                   error: function() {
                       alert('Error occured');
                   }
                 

               });
              
            }
      })
      }
      
   $(".additem").click(function(e){
  
     e.preventDefault();
     var counter = 0;

     var data = $("#selectbox1").val();
     var url = window.location.origin;
       var tr = $(".tbody>tr");
       for(i=0 ;i<tr.length-1;i++ ){
         counter++;
       }

       var newRow = `<tr>
                                   <td style="width:200px">
                                     <div class="form-group">
                                       <input type="search" class="sellProduct sellProduct-${counter} form-control" >
                                       <ul class="ul d-none"></ul>
                                     </div>
                                   </td>
                                   <td>
                                     <input type="number" class="form-control av-cartoon"  name="av_cartoon[]" readonly>
                                   </td>
                                   <td>
                                     <input type="number" class="form-control cartoon"  name="cartoon[]">
                                   </td>
                                   <td>
                                     <input type="text" class="form-control ppc"  name="ppc[]" readonly>
                                   </td>
                                   <td>
                                     <input type="text" class="form-control qty"  name="qty[]" readonly> 
                                   </td>
                                   <td>
                                     <input type="text" class="form-control rate"  name="rate[]" >
                                   </td>
                                   <td>
                                     <input type="text" class="form-control total" name="total[]" readonly>
                                   </td>
                                   <td>
                                    <button type="button" class="btn btn-danger delete-${counter}">Delete</button>
                                   </td>
                                 </tr>`;
       $(".tbody").append(newRow);
       
        $('.sellProduct-'+counter).keyup(function(){
       
        var data = $(this).val();
        var url = window.location.origin;
        var ul = $(this).siblings();
       ul.removeClass('d-none');
            if(data != 0){
             
                   $.ajax({
                   type:"GET",
                   url : url+'/Inventory/product-get/'+data,
                   data : {data:data},
                   success : function(response) {
                      ul.children().remove();
                       data = response;
                       console.log(data);
                       
                       $.each (data, function (key, val) {
                         
                         var liTag = `<li class='li' value="${val.id}"> ${val.name}</li>`
                         ul.append(liTag);

                       });

                       $('.li').click(function(){
                         var id = $(this).val();
                         var inputs = $(this).parents("td").siblings();
                         var siblingsOfParent = $(this).parent().siblings();
                         siblingsOfParent.val($(this).text());
                         var hiddenInput = siblingsOfParent.parent();
                         hiddenInput.append(`<input type="search" class="sellProduct form-control" hidden name="product_id[]" value = "${id}">`);

                         ul.children().remove();
                         ul.addClass('d-none');

                         $.each(data, function (key, val) 
                         {

                             if(id == val.id)
                             {
                               
                               inputs.find('.av-cartoon').val(val.cartoon);
                               var cartoon = inputs.find('.cartoon');
                               inputs.find('.ppc').val(val.ppc);
                               cartoon.keyup(function(){

                                 var thisCartoon = $(this);
                                 if(thisCartoon.val() < 0)
                                 {
                                   thisCartoon.val(0);
                                 }
                                 if(thisCartoon.val() >= val.cartoon)
                                 {
                                   thisCartoon.val(val.cartoon);
                                 }
                                 inputs.find('.av-cartoon').val(val.cartoon - thisCartoon.val());
                                 var rate = inputs.find('.rate');
                                 var qty = inputs.find('.qty');
                                 // var sellPrice = val.sell_price;
                                     inputs.find('.qty').val($(this).val()*val.ppc);
                                     rate.keyup(function(){
                                         sellPrice = $(this).val()*qty.val();
                                         inputs.find('.total').val(Math.round(sellPrice));
                                     })
                                     inputs.find('.total').val(Math.round(qty.val()*rate.val()));
                                     var grndTotal = $('.total');
                                     console.log(grndTotal);
                                     var grndtotal = 0;
                                     $.each(grndTotal,function(key,value){
                                       grndtotal += parseInt(value.value);
                                     })
                                     $('.grandTotal').val(grndtotal);

                                     if ($(this).val() != 0) {
                                         $('.sbmt').attr('disabled',false);
                                         }else{
                                           $('.sbmt').attr('disabled',true);
                                         }

                               })
                               var rt = (val.sell_price/val.ppc).toFixed(3);
                               inputs.find('.rate').val(rt);
                     
                              
                             }
                         });

                       })
                   },
                   error: function() {
                       alert('Error occured');
                   }
               });
            }
      })
      
   $(".delete-"+counter).click(function(){
   
    $(".delete-"+counter).closest("tr").remove();
     counter--;
     
   })
      
   });
   // sellProduct();
   //--------------------------------------add click function end-----------------------------------------------
     
//sellProduct function call
       sellProduct();
      
      //sellProduct function call


      $('.paid').keyup(function(){
           gtotal =  $('.grandTotal').val();
           if(gtotal != 0){

           $('.due').val( gtotal - $(this).val());
           }
      })

   // var grnd =  $('.grandTotal').value;
   // console.log(grnd);
   
   // if (grnd == 0 || grnd == null ) {
   //   $('.sbmt').attr('disabled',true);
   // }else{
   //   $('.sbmt').attr('disabled',false);

   // }

     
});