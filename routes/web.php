<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('admin.dashboard');

Route::group(['prefix' => 'Inventory','namespace'=>'Inventory'], function () {
    Route::resource('/category', 'CategoryController');
    Route::resource('/supplier', 'SupplierController');
    Route::resource('/supplier-product', 'SupplierProductController');
    Route::resource('/purchase', 'PurchaseController');
    Route::get('/purchase-item/{id}','PurchaseController@supplierItem')->name('supplier.item');
    
    Route::get('/customer/credit','CustomerController@creditCustomer')->name('customer.creditCustomer');
    Route::resource('/customer', 'CustomerController');
    Route::get('/customer-paid','CustomerController@paidCustomer')->name('customer.paidCustomer');
    Route::get('/product','PurchaseController@products')->name('product.index');
    Route::resource('/sell', 'SellController');
    Route::get('/product-get/{name}','SellController@getProduct');
    Route::get('/sell-paid','SellController@getPaid')->name('sell.paid');
    Route::get('/sell-due','SellController@getDue')->name('sell.due');
   
});