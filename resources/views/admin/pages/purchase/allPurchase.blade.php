@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Purchase Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped table-responsive-sm">
                            <thead>
                            <tr>
                              <th>serial</th>
                              <th> Purchase no</th>
                              <th>Supplier Name</th>
                              <th>Product</th>
                              <th>Purchase Date</th>
                              <th>Total Amount</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($purchases as $key => $purchase)
                            <tr class="text-center">
                                <td>{{$key+1}}</td>
                                <td>{{$purchase->purchase_no}}</td>
                                <td>{{$purchase->supplier->name}}</td>
                                <td>
                                  @foreach ($purchase->SupplierProducts as $product)
                                    <div class="badge badge-primary">{{$product->name}}</div>
                                  @endforeach
                              </td>
                                <td>{{$purchase->purchase_date}}</td>
                                <td>{{$purchase->grand_total}}</td>
                                <td>
                                <a data-id="{{$purchase->id}}" class="btn btn-success float-left mr-2 viewModalBtn" data-toggle="modal" data-target="#view" >View</a>

                                <form action="{{route('purchase.destroy',$purchase->id)}}" method="POST">
                                  @csrf
                                  @method("DELETE")
                                          <button class="btn btn-danger" onclick="return confirm('are you sure ?')">Delete</button>
                                    </form>
                                </td>
                              </tr>
                            @endforeach    
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->

                                  <!--View Modal -->
              <div class="modal fade"  id="view" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Category</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <table class="table table-default table-responsive-sm text-center">
                      <thead>
                        <tr>
                          <th scope="col">S.L</th>
                          <th scope="col">Product</th>
                          <th scope="col">Image</th>
                          <th scope="col">Cartoon</th>
                          <th scope="col">PPC</th>
                          <th scope="col">Quantity</th>
                          <th scope="col">Rate</th>
                          <th scope="col">Total</th>
                          
                        </tr>
                      </thead>
                      <tbody id="productView">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                      </div>
                </div>
            </div>
            
             
            

        </div>
    </div>
@endsection

@push('js')
    <script>
      $(document).ready(function() {

          $.ajaxSetup({

          headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

          }

    });
    $('.viewModalBtn').click(function(e) {
      e.preventDefault();
      
      // alart('hello');
      var data=$(this).data('id');
      // var action ="{{ route('category.edit',"id") }}";
      var url = window.location.origin;
      // var url = '{{URL::to('class_routine')}}';
      $.ajax({
        type : 'GET',
        url : url+'/Inventory/purchase/'+data,
        data : {'id':data},
        
        success:function(data){

          // $('#cat_name').val(data.cat_name);
          var td = "";
          

          for(var i = 0 ;i<data.length;i++)
          {
            td += 
           `
              <tr>
                <th scope="row">${i+1}</th>
                <td>${data[i].name}</td>
                <td><img src="{{url('storage/productimage/${data[i].product_img}')}}" alt="" class="img-fluid "></td>
                <td>${data[i].pivot.cartoon}</td>
                <td>${data[i].ppc}</td>
                <td>${data[i].pivot.qty}</td>
                <td>${data[i].pivot.rate}</td>
                <td>${data[i].pivot.total}</td>    
              </tr>
              `

          }
          $("#productView").html(td);
          console.log(data);
       
          // $('.class-id').val(data.class_id);
          // $('.subject_id').val(data.subject_id);
          // $('.day_of_week').val(data.day_of_week);
          // $('.teacher_id').val(data.teacher_id);
          // $('.start_time').val(data.start_time);
          // $('.end_time').val(data.end_time);
          // $('.classFormUpdate').attr('action',action);
          $('#editModal').modal('show');
        }
      });
    });
});
    </script>
@endpush