@extends('home')
@push('css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<style>
  /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
@endpush
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class=" col-md-12 mt-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">New Purchase</h3>
                      </div>
                      <!-- /.card-header -->
                      <!-- form start -->
                     <form role="form" action="{{route('purchase.store')}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          <div class="card-body">
                            <div class="row">
                               <div class="col-6">
                                <div class="form-group">
                                  <label>Suppliers</label>
                                  <select class="form-control select2" style="width: 100%;" name="supplier" id="selectbox1">
                                    <option selected="selected">--select suppliers--</option>
                                    @foreach ($suppliers as $item)
                                           <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Purchase Date</label>
                                    <input type="date" class="form-control" id="exampleInputEmail1" name="purchase_date">
                                  </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Purchase No</label>
                                    <input type="number" class="form-control" id="exampleInputEmail1" name="purchase_no">
                                  </div>
                              </div>
                              <div class="col-3">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Purchase details</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="details">
                                  </div>
                              </div>
                              <div class="col-3">
                                  <div class="form-group">
                                    <label for="">Payment Type</label>
                                    <select class="form-control" name="payment_type" id="">
                                      <option>Select</option>
                                      <option value="cash">Cash <i class="fas fa-money-bill-alt text-dark"></i> </option>
                                      <option value="card">Card <i class="fas fa-money-bill-alt"></i></option>
                                    </select>
                                  </div>
                              </div>
                              <div class="col-12">
                                <table class="table table-bordered table-striped text-center">
                                  <thead>
                                  <tr>
                                    <th>Product name</th>
                                    <th>Cartoon</th>
                                    <th>PPC</th>
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                  </tr>
                                  </thead>
                                  <tbody class="tbody">
                                  <tr class="firsttr" >
                                    <td style="width:200px">
                                      <div class="form-group">
                                        <select class="form-control select2 supItems" style="width: 100%;" name="product_id[]" >
                                         <option selected>select product</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <input type="number" class="form-control cartoon" id="exampleInputEmail1" name="cartoon[]">
                                    </td>
                                    <td>
                                      <input type="text" class="form-control ppc" id="exampleInputEmail1" name="ppc[]" readonly>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control qty" id="exampleInputEmail1" name="qty[]" readonly> 
                                    </td>
                                    <td>
                                      <input type="text" class="form-control rate" id="exampleInputEmail1" name="rate[]" >
                                    </td>
                                    <td>
                                      <input type="text" class="form-control total" id="exampleInputEmail1" name="total[]" readonly>
                                    </td>
                                    <td>
                                     <button type="button" class="btn btn-danger" >Delete</button>
                                    </td>
                                  </tr>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td colspan="2">
                                      <button type="button" class="btn btn-success float-left additem" >Add Item</button>
                                    </td>
                                    <td colspan="3" class=""><span class="float-right h5 text-bold">Grand Total :</span></td>
                                    <td>
                                      <input type="text" class="form-control grandTotal" id="exampleInputEmail1" name="grand_total" readonly>
                                    </td>
                                  </tr>
                                </tfoot>

                              </table>
                              </div>
                            </div>
                          </div>
                        <!-- /.card-body -->
        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.card -->
                  </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

<script>
   $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

    //Initialize Select2 Elements
      $('.select2bs4').select2({
    theme: 'bootstrap4'
    })

    
    $(".additem").click(function(e){
   
      e.preventDefault();
      var counter = 0;

      var data = $("#selectbox1").val();
      var url = window.location.origin;

      $.ajax({
        type:"GET",
        url : url+'/Inventory/purchase-item/'+data,
        data : {data:data},
        success : function(response) {
          var res ="";
            data = response;
            $.each (data, function (key, value) {
              if(key == 0){
                res += '<option  selected >Select Product</option>';
             }
            res +=  '<option value = "'+value.id+'">'+value.name+'</option>';
            });
           $(".supItems-"+counter).html(res);

            supItemSystem(data);

        },
        error: function() {
            alert('Error occured');
        }
    });

        var tr = $(".tbody>tr");
        for(i=0 ;i<tr.length-1;i++ ){
          counter++;
        }

        var newRow = `<tr>
                                    <td style="width:200px">
                                      <div class="form-group">
                                        <select class="form-control select2 supItems-${counter} supItems" style="width: 100%;" name="product_id[]">
                                          <option selected>select product</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <input type="number" class="form-control cartoon" id="exampleInputEmail1" name="cartoon[]">
                                    </td>
                                    <td>
                                      <input type="text" class="form-control ppc" id="exampleInputEmail1" name="ppc[]" readonly>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control qty" id="exampleInputEmail1" name="qty[]" readonly> 
                                    </td>
                                    <td>
                                      <input type="text" class="form-control rate" id="exampleInputEmail1" name="rate[]" >
                                    </td>
                                    <td>
                                      <input type="text" class="form-control total" id="exampleInputEmail1" name="total[]" readonly>
                                    </td>
                                    <td>
                                     <button type="button" class="btn btn-danger delete-${counter}">Delete</button>
                                    </td>
                                  </tr>`;
        $(".tbody").append(newRow);
       
    $(".delete-"+counter).click(function(){
    
     $(".delete-"+counter).closest("tr").remove();
      counter--;
      
    })

        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
          $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    });
   
    



  // ajax call from select box
  

 
    $('#selectbox1').change(function() {
    var data = $(this).val();
    var url = window.location.origin;
    $('.grandTotal').val('');

    var counter = 0;

    var tr = $(".supItems").closest('tr');
              tr.find(".cartoon").val("");
              tr.find(".qty").val("");
              tr.find(".total").val("");
              tr.find(".ppc").val("");
              tr.find(".rate").val("");

    $.ajax({
        type:"GET",
        url : url+'/Inventory/purchase-item/'+data,
        data : {data:data},
        success : function(response) {
          var res ="";
            data = response;
            $.each (data, function (key, value) {
             if(key == 0){
              res += '<option  selected >Select Product</option>';
             }
              res += '<option value = "'+value.id+'">'+value.name+'</option>';
            });
            supItemSystem(data);
           $(".supItems").html(res);

        },
        error: function() {
            alert('Error occured');
        }
    });
});

function supItemSystem(data){

 return ( $(".supItems").change(function(){
              var itemId = $(this).val();
              var tr = $(this).closest('tr');
              tr.find(".cartoon").val("");
              tr.find(".qty").val("");
              tr.find(".total").val("");
              $.each(data,function(key,value){
                if(value.id == itemId)
                {
                   tr.find(".ppc").val(value.ppc);
                    var rate = (value.supplier_price/value.ppc).toFixed(3);
                    tr.find(".rate").val(rate);
                   tr.find(".cartoon").keyup(function(){
                    var cartn = $(this).val();
                    var ppc = tr.find(".ppc").val();
                    tr.find(".qty").val(cartn*ppc);
                   var qty = tr.find(".qty").val();
                   var rate = tr.find(".rate").val();
                   tr.find(".total").val(Math.round(qty*rate));

                   var grndTotal = $('.total');
                    var grndtotal = 0;
                    $.each(grndTotal,function(key,value){
                    grndtotal += parseInt(value.value);
                    
                    })
                   $('.grandTotal').val(grndtotal);
                    console.log(grndtotal);
                   })
                }
              })
            })
 );
                
}
  })
</script>

@endpush
 
