@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-3 col-md-6 mt-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">Create Supplier</h3>
                      </div>
                      <!-- /.card-header -->
                      <!-- form start -->
                     <form role="form" action="{{route('supplier.store')}}" method="POST">
                          @csrf
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Supplier Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="name">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Mobile</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="mobile">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="address">
                          </div>
                          <div class="form-group">
                            <label for="">Details</label>
                            <textarea class="form-control" name="details" id="" rows="3"></textarea>
                          </div>
                        </div>
                        <!-- /.card-body -->
        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.card -->
                  </div>
            </div>
        </div>
    </div>
@endsection

