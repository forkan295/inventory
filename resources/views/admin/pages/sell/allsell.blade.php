@extends('home')
@push('css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<style>
  /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
.li{
  list-style: none;
  cursor: pointer;
  border-bottom: 1px solid rgb(248, 248, 248);
  
}
.li:hover
{
  color: white;
  
}
.li:last-child{
  border-bottom: none;
}
.ul{
  padding: 0;
  background-color: rgba(187, 187, 187, 0.678);
  border-bottom: 1px solid rgba(43, 43, 43, 0.301);
  border-left: 1px solid rgb(43, 43,43, 0.301);
  border-right: 1px solid rgb(43, 43,43, 0.301);
  border-radius: 0px 0px 5px 5px;

 
}
</style>
@endpush
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Sell Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped table-responsive-sm">
                            <div class="text-center">
                            <a href="{{route('sell.index')}}" class="btn btn-outline-dark mb-3 pl-4 pr-4 all" >All</a>
                              <a href="{{route('sell.paid')}}" class="btn btn-outline-dark mb-3 pl-4 pr-4 paid" >Paid</a>
                              <a href="{{route('sell.due')}}" class="btn btn-outline-dark mb-3 pl-4 pr-4 due">Due</a>
                              
                            </div>
                            <thead>
                            <tr>
                              <th>serial</th>
                              {{-- <th> Purchase no</th> --}}
                              <th>Customer Name</th>
                              <th>Product</th>
                              <th>Sell Date</th>
                              <th>Total Amount</th>
                              <th>Due</th>
                              <th>Paid</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sells as $key => $sell)
                            <tr class="text-center">
                                <td>{{$key+1}}</td>
                                {{-- <td>{{$purchase->purchase_no}}</td> --}}
                                <td>{{$sell->Customer->name}}</td>
                                <td>
                                  @foreach ($sell->Products as $product)
                                    <div class="badge badge-primary">{{$product->name}}</div>
                                  @endforeach
                              </td>
                                <td>{{$sell->date}}</td>
                                <td>{{$sell->grand_total}}</td>
                                <td>{{$sell->due}}</td>
                                <td>{{$sell->paid}}</td>
                                <td>
                                <a data-id="{{$sell->id}}" class="btn btn-success float-left mr-2 viewModalBtn" data-toggle="modal" data-target="#view" >View</a>
                                <a data-id="{{$sell->id}}" class="btn btn-primary text-white float-left mr-2 editModalBtn" data-toggle="modal" data-target="#edit" >Edit</a>
                                <form action="{{route('sell.destroy',$sell->id)}}" method="POST">
                                  @csrf
                                  @method("DELETE")
                                          <button class="btn btn-danger" onclick="return confirm('are you sure ?')">Delete</button>
                                    </form>
                                </td>
                              </tr>
                            @endforeach    
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->

              
             {{-- edit modal --}}
              <div class="modal fade"  id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                  <div class="modal-content" >
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Sell</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form role="form" action="{{route('sell.store')}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="card-body">
                        <div class="row">
                           <div class="col-6">
                            <div class="form-group">
                              <label>Customer</label>
                              <select class="form-control select2" style="width: 100%;" name="customer" id="selectbox1">
                                {{-- <option selected="selected" value="{{$sell->Customer->id}}">{{$sell->Customer->name}}</option> --}}

                              </select>
                            </div>
                          </div>
                          <div class="col-6">
                              <div class="form-group">
                                <label for="exampleInputEmail1"> Date</label>
                               <input type="date" class="form-control date" name="date" value="">
                              </div>
                          </div>
                          
                          <div class="col-6">
                              <div class="form-group">
                                <label for="exampleInputEmail1"> Details</label>
                                <input type="text" class="form-control details" name="details">
                              </div>
                          </div>
                          <div class="col-6">
                              <div class="form-group">
                                <label for="">Payment Type</label>
                                <select class="form-control payment" name="payment_type" id="">

                                </select>
                              </div>
                          </div>
                          <div class="col-12">
                            <table class="table table-bordered table-striped text-center">
                              <thead>
                              <tr>
                                <th>Product name</th>
                                <th> Available Cartoon</th>
                                <th>cartoon</th>
                                <th>PPC</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Total</th>
                                <th>Action</th>
                              </tr>
                              </thead>
                              <tbody class="tbody">
                               
                              </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="2">
                                  <button type="button" class="btn btn-success float-left additem" >Add Item</button>
                                </td>
                                <td colspan="4" class="">
                                  <span class="float-right h5 text-bold">Grand Total :</span>
                                </td>
                                <td>
                                  <input type="text" class="form-control grandTotal"  name="grand_total" readonly>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="6">
                                  <span class="float-right h5 text-bold">Paid :</span>
                                </td>
                                <td colspan="0">
                                  <input type="text" class="form-control paid"  name="paid">
                                </td>
                              </tr>
                              <tr>
                                <td colspan="6">
                                  <span class="float-right h5 text-bold">Due :</span>
                                </td>
                                <td colspan="0">
                                  <input type="text" class="form-control due"  name="due">
                                </td>
                              </tr>
                            </tfoot>

                          </table>
                          </div>
                        </div>
                      </div>
                    <!-- /.card-body -->
    
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary sbmt" >Submit</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>

              

                                  <!--View Modal -->
                                  <div class="modal fade"  id="view" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                      <div class="modal-content" style="width:700px">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLongTitle">View Sell</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <table class="table table-default table-responsive-sm text-center">
                                          <thead>
                                            <tr>
                                              <th scope="col">S.L</th>
                                              <th scope="col">Product</th>
                                              <th scope="col">Image</th>
                                              <th scope="col">Cartoon</th>
                                              <th scope="col">PPC</th>
                                              <th scope="col">Quantity</th>
                                              <th scope="col">Rate</th>
                                              <th scope="col">Total</th>
                                              
                                            </tr>
                                          </thead>
                                          <tbody id="productView">
                    
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                      </div>
                </div>
            </div>
            
             
            

        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset("js/sell.js")}}"></script>
    <script src="{{asset("js/allsell.js")}}"></script>
     
@endpush