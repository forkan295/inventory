@extends('home')
@push('css')
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<style>
  /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
.li{
  list-style: none;
  cursor: pointer;
  border-bottom: 1px solid rgb(248, 248, 248);
  
}
.li:hover
{
  color: white;
  
}
.li:last-child{
  border-bottom: none;
}
.ul{
  padding: 0;
  background-color: rgba(187, 187, 187, 0.678);
  border-bottom: 1px solid rgba(43, 43, 43, 0.301);
  border-left: 1px solid rgb(43, 43,43, 0.301);
  border-right: 1px solid rgb(43, 43,43, 0.301);
  border-radius: 0px 0px 5px 5px;

 
}

</style>
@endpush
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class=" col-md-12 mt-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">New Sell</h3>
                      </div>
                      <!-- /.card-header -->
                      <!-- form start -->
                     <form role="form" action="{{route('sell.store')}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          <div class="card-body">
                            <div class="row">
                               <div class="col-6">
                                <div class="form-group">
                                  <label>Customer</label>
                                  <select class="form-control select2" style="width: 100%;" name="customer" id="selectbox1">
                                    <option selected="selected">Select Customer</option>
                                    @foreach ($customers as $item)
                                           <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1"> Date</label>
                                    <input type="date" class="form-control" name="date">
                                  </div>
                              </div>
                              
                              <div class="col-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1"> Details</label>
                                    <input type="text" class="form-control" name="details">
                                  </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                    <label for="">Payment Type</label>
                                    <select class="form-control" name="payment_type" id="">
                                      <option>Select</option>
                                      <option value="cash">Cash <i class="fas fa-money-bill-alt text-dark"></i> </option>
                                      <option value="card">Card <i class="fas fa-money-bill-alt"></i></option>
                                    </select>
                                  </div>
                              </div>
                              <div class="col-12">
                                <table class="table table-bordered table-striped text-center">
                                  <thead>
                                  <tr>
                                    <th>Product name</th>
                                    <th> Available Cartoon</th>
                                    <th>cartoon</th>
                                    <th>PPC</th>
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                  </tr>
                                  </thead>
                                  <tbody class="tbody">
                                  <tr class="firsttr" >
                                    <td style="width:200px">
                                      <div class="form-group">
                                        <input type="search" class="sellProduct form-control">

                                        <ul class="ul d-none"></ul>
                                      </div>
                                    </td>
                                    <td>
                                      <input readonly type="number" class="form-control av-cartoon" name="av_cartoon[]">
                                    </td>
                                    <td>
                                      <input  type="number" class="form-control cartoon" name="cartoon[]">
                                    </td>
                                    <td>
                                      <input type="text" class="form-control ppc" name="ppc[]" readonly>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control qty"  name="qty[]" readonly> 
                                    </td>
                                    <td>
                                      <input type="text" class="form-control rate" name="rate[]" >
                                    </td>
                                    <td>
                                      <input type="text" class="form-control total"  name="total[]" readonly>
                                    </td>
                                    <td>
                                     <button type="button" class="btn btn-danger" >Delete</button>
                                    </td>
                                  </tr>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td colspan="2">
                                      <button type="button" class="btn btn-success float-left additem" >Add Item</button>
                                    </td>
                                    <td colspan="4" class="">
                                      <span class="float-right h5 text-bold">Grand Total :</span>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control grandTotal"  name="grand_total" readonly>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="6">
                                      <span class="float-right h5 text-bold">Paid :</span>
                                    </td>
                                    <td colspan="0">
                                      <input type="text" class="form-control paid"  name="paid">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="6">
                                      <span class="float-right h5 text-bold">Due :</span>
                                    </td>
                                    <td colspan="0">
                                      <input type="text" class="form-control due"  name="due">
                                    </td>
                                  </tr>
                                </tfoot>

                              </table>
                              </div>
                            </div>
                          </div>
                        <!-- /.card-body -->
        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary sbmt" disabled>Submit</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.card -->
                  </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
  <script src="{{asset("js/sell.js")}}"></script>
@endpush
 
