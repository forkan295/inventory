@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-3 col-md-6 mt-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">Create Supplier Product</h3>
                      </div>
                      <!-- /.card-header -->
                      <!-- form start -->
                     <form role="form" action="{{route('supplier-product.store')}}" method="POST" enctype="multipart/form-data">
                          @csrf
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Product Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="name">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Product Model</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="model_name">
                          </div>
                          <div class="form-group">
                            <label for="">Category</label>
                            <select class="form-control" name="product_category" id="">
                              <option>--select--</option>
                              @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->cat_name}}</option>
                              @endforeach
                             
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Product Per Cartoon</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="ppc">
                          </div>
                          <div class="form-group">
                            <div class="form-group">
                              <label for="">Suppliers Name</label>
                              <select class="form-control" name="supplier_name[]" id="" multiple>
                                <option>--suppliers</option>
                                @foreach ($suppliers as $supplier)
                              <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                @endforeach
                                
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Suppliers Price</label>
                            <input type="number" class="form-control" id="exampleInputEmail1" name="supplier_price">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Sell Price</label>
                            <input type="number" class="form-control" id="exampleInputEmail1" name="sell_price">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1"> Product image</label>
                            <input type="file" class="form-control" id="exampleInputEmail1" name="product_img">
                          </div>
                        </div>
                        <!-- /.card-body -->
        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.card -->
                  </div>
            </div>
        </div>
    </div>
@endsection

