@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Supplier Product Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped table-responsive-sm">
                            <thead>
                            <tr>
                              <th>serial</th>
                              <th> Name</th>
                              <th>Model Name</th>
                              <th>Product per Cartoon</th>
                              <th>category</th>
                              <th>Supplier Name</th>
                              <th>Supplier Price</th>
                              <th>ppp</th>
                              <th>Sell Price</th>
                              <th>Product Image</th>
                              <th>Action</th>
                             
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $key => $product)
                            <tr class="text-center">
                                <td>{{$key+1}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->model_name}}</td>
                                <td>{{$product->ppc}}</td>
                                <td>{{$product->category->cat_name}}</td>
                                <td>@foreach ($product->suppliers as $supplier)
                                    <div class="badge badge-primary">{{$supplier->name}}</div>
                                @endforeach</td>
                                <td>{{$product->supplier_price}}</td>
                                <td>{{round($product->supplier_price/$product->ppc)}}</td>
                                <td>{{$product->sell_price}}</td>
                                <td style="width:200px">
                                <img src="{{url('storage/productimage/'.$product->product_img)}}" alt="" class="img-fluid ">
                                </td>
                                <td>
                                <a href="" data-id="{{$product->id}}" class="btn btn-success float-left mr-2 editModalBtn" data-toggle="modal" data-target="#exampleModalCenter{{$product->id}}" >Edit</a>
                                    {{-- data-toggle="modal" data-target="#exampleModalCenter" --}}
                                <form action="{{route('supplier-product.destroy',$product->id)}}" method="POST">
                                  @csrf
                                  @method("DELETE")
                                          <button class="btn btn-danger" onclick="return confirm('are you sure ?')">Delete</button>
                                    </form>
                                </td>
                              </tr>


                               <!-- Modal -->
              <div class="modal fade" id="exampleModalCenter{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Category</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form role="form" action="{{route('supplier-product.update',$product->id)}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      @method("PUT")
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Product Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{$product->name}}">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Product Model</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="model_name" value="{{$product->model_name}}">
                      </div>
                      <div class="form-group">
                        <label for="">Category</label>
                        <select class="form-control" name="product_category" id="">
                          <option>--select--</option>
                          @foreach ($categories as $category)
                        <option
                        @if ($category->id == $product->category->id)
                            selected
                        @endif
                         value="{{$category->id}}"
                         >{{$category->cat_name}}
                        </option>
                          @endforeach
                         
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Product Per Cartoon</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="ppc" value="{{$product->ppc}}">
                      </div>
                      <div class="form-group">
                        <div class="form-group">
                          <label for="">Suppliers Name</label>
                          <select class="form-control" name="supplier_name[]" id="" multiple>
                            <option>--suppliers</option>
                            @foreach ($suppliers as $supplier)
                          <option 
                          @foreach ($product->suppliers as $item)
                              
                          {{$supplier->id == $item->id ? 'selected' : ''}}
                          @endforeach
                          value="{{$supplier->id}}">
                          {{$supplier->name}}
                        </option>
                            @endforeach
                            
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Suppliers Price</label>
                      <input type="number" class="form-control" id="exampleInputEmail1" name="supplier_price" value="{{$product->supplier_price}}">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Sell Price</label>
                        <input type="number" class="form-control" id="exampleInputEmail1" name="sell_price" value="{{$product->sell_price}}">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1"> Product image</label>
                        <input type="file" class="form-control" id="exampleInputEmail1" name="product_img">
                      </div>
                    </div>
                    <!-- /.card-body -->
    
                    <div class="card-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
                            @endforeach
                           
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
            </div>
            
             
            

        </div>
    </div>
@endsection

@push('js')
    <script>
//       $(document).ready(function() {

//           $.ajaxSetup({

//           headers: {

//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

//           }

//     });
//     $('.editModalBtn').click(function(e) {
//       e.preventDefault();
      
//       // alart('hello');
//       var id=$(this).data('id');
//       // var action ="{{ route('category.edit',"id") }}";
//       var url = '{{ route("category.edit", ":id") }}';
//     	url = url.replace(':id',id);
//       // var url ='{{url("/Inventory/category'+id+'/edit")}}';
//       // var action = url('/Inventory/category/'+id+'/edit');
//       console.log(url);


//       // var url = '{{URL::to('class_routine')}}';
//       $.ajax({
//         type : 'GET',
//         url  : url,
//         data : {'id':id},
        
//         success:function(data){

//           // $('#cat_name').val(data.cat_name);
//           console.log(data);
//           // $('.class-id').val(data.class_id);
//           // $('.subject_id').val(data.subject_id);
//           // $('.day_of_week').val(data.day_of_week);
//           // $('.teacher_id').val(data.teacher_id);
//           // $('.start_time').val(data.start_time);
//           // $('.end_time').val(data.end_time);
//           // $('.classFormUpdate').attr('action',action);
//           $('#editModal').modal('show');
//         }
//       });
//     });
// });
    </script>
@endpush