@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">All Customers</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>serial</th>
                              <th>Name</th>
                              <th>Mobile</th>
                              <th>Address</th>
                              <th>Email</th>
                              <th>Due</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers as $key => $customer)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->mobile}}</td>
                                <td>{{$customer->address}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->due}}</td>
                                <td>
                                <a href="" data-id="{{$customer->id}}" class="btn btn-success float-left mr-2 editModalBtn" data-toggle="modal" data-target="#exampleModalCenter{{$customer->id}}" >Edit</a>
                                    {{-- data-toggle="modal" data-target="#exampleModalCenter" --}}
                                <form action="{{route('customer.destroy',$customer->id)}}" method="POST">
                                  @csrf
                                  @method("DELETE")
                                          <button class="btn btn-danger" onclick="return confirm('are you sure ?')">Delete</button>
                                    </form>
                                </td>
                              </tr>
                               <!-- Modal -->
              <div class="modal fade" id="exampleModalCenter{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Customer</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form role="form" action="{{route('customer.update',$customer->id)}}" method="POST">
                      @csrf
                      @method("PUT")
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Customer Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$customer->name}}" name="name">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mobile</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$customer->mobile}}" name="mobile">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" value="{{$customer->email}}" name="email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$customer->address}}" name="address">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Due</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$customer->due}}" name="due">
                      </div>
                     
                    </div>
                    <!-- /.card-body -->
    
                    <div class="card-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
                            @endforeach
                           
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
            </div>
            
             
            

        </div>
    </div>
@endsection

@push('js')
    <script>
//       $(document).ready(function() {

//           $.ajaxSetup({

//           headers: {

//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

//           }

//     });
//     $('.editModalBtn').click(function(e) {
//       e.preventDefault();
      
//       // alart('hello');
//       var id=$(this).data('id');
//       // var action ="{{ route('category.edit',"id") }}";
//       var url = '{{ route("category.edit", ":id") }}';
//     	url = url.replace(':id',id);
//       // var url ='{{url("/Inventory/category'+id+'/edit")}}';
//       // var action = url('/Inventory/category/'+id+'/edit');
//       console.log(url);


//       // var url = '{{URL::to('class_routine')}}';
//       $.ajax({
//         type : 'GET',
//         url  : url,
//         data : {'id':id},
        
//         success:function(data){

//           // $('#cat_name').val(data.cat_name);
//           console.log(data);
//           // $('.class-id').val(data.class_id);
//           // $('.subject_id').val(data.subject_id);
//           // $('.day_of_week').val(data.day_of_week);
//           // $('.teacher_id').val(data.teacher_id);
//           // $('.start_time').val(data.start_time);
//           // $('.end_time').val(data.end_time);
//           // $('.classFormUpdate').attr('action',action);
//           $('#editModal').modal('show');
//         }
//       });
//     });
// });
    </script>
@endpush