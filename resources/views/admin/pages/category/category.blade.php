@extends('home')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-3 col-md-6 mt-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">Create Category</h3>
                      </div>
                      <!-- /.card-header -->
                      <!-- form start -->
                     <form role="form" action="{{route('category.store')}}" method="POST">
                          @csrf
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Category name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="cat_name">
                          </div>
                        </div>
                        <!-- /.card-body -->
        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.card -->
                  </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Categories Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>serial</th>
                              <th>Category Name</th>
                              <th>Action</th>
                             
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $key => $category)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$category->cat_name}}
                                </td>
                                <td>
                                <a href="" data-id="{{$category->id}}" class="btn btn-success float-left mr-2 editModalBtn" data-toggle="modal" data-target="#exampleModalCenter{{$category->id}}" >Edit</a>
                                    {{-- data-toggle="modal" data-target="#exampleModalCenter" --}}
                                <form action="{{route('category.destroy',$category->id)}}" method="POST">
                                  @csrf
                                  @method("DELETE")
                                          <button class="btn btn-danger" onclick="return confirm('are you sure ?')">Delete</button>
                                    </form>
                                </td>
                              </tr>


                               <!-- Modal -->
              <div class="modal fade" id="exampleModalCenter{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Category</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  <form action="{{route('category.update',$category->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Category name</label>
                        <input type="text" class="form-control" id="cat_name" name="cat_name" value="{{$category->cat_name}}">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                            @endforeach
                           
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
            </div>
            
             
            

        </div>
    </div>
@endsection

@push('js')
    <script>
//       $(document).ready(function() {

//           $.ajaxSetup({

//           headers: {

//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

//           }

//     });
//     $('.editModalBtn').click(function(e) {
//       e.preventDefault();
      
//       // alart('hello');
//       var id=$(this).data('id');
//       // var action ="{{ route('category.edit',"id") }}";
//       var url = '{{ route("category.edit", ":id") }}';
//     	url = url.replace(':id',id);
//       // var url ='{{url("/Inventory/category'+id+'/edit")}}';
//       // var action = url('/Inventory/category/'+id+'/edit');
//       console.log(url);


//       // var url = '{{URL::to('class_routine')}}';
//       $.ajax({
//         type : 'GET',
//         url  : url,
//         data : {'id':id},
        
//         success:function(data){

//           // $('#cat_name').val(data.cat_name);
//           console.log(data);
//           // $('.class-id').val(data.class_id);
//           // $('.subject_id').val(data.subject_id);
//           // $('.day_of_week').val(data.day_of_week);
//           // $('.teacher_id').val(data.teacher_id);
//           // $('.start_time').val(data.start_time);
//           // $('.end_time').val(data.end_time);
//           // $('.classFormUpdate').attr('action',action);
//           $('#editModal').modal('show');
//         }
//       });
//     });
// });
    </script>
@endpush